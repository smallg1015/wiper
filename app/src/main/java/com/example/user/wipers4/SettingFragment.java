package com.example.user.wipers4;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;


public class SettingFragment extends Fragment {
    private SharedPreferences prelogin;
    private ListView list_Setting;
    private File filefolder=new File(Environment.getExternalStorageDirectory().getPath());
    private String txt_foldernow="555";
    private String listst_foldernow;
    private Handler handler=new Handler();

    private SharedPreferences presavefileadr;
    String[] list_head=new String[]{"設定儲存位置","登出"};
    String[] list_sub;
    Myadapter myadapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_setting, container, false);
    }

    @Override
    public void onViewCreated(View view,Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView title=(TextView)getActivity().findViewById(R.id.action_bar_title);
        title.setText("設定");







        presavefileadr=getActivity().getSharedPreferences("SaveAddress",Context.MODE_PRIVATE);
        prelogin=getActivity().getSharedPreferences("prelogin",Context.MODE_PRIVATE);


        String loadsaveaddress=presavefileadr.getString("Savefile","");
        listst_foldernow=loadsaveaddress;

        list_sub=new String[]{listst_foldernow,""};



        list_Setting=(ListView)getView().findViewById(R.id.list_Setting);

        myadapter=new Myadapter(getActivity());
        list_Setting.setAdapter(myadapter);

        list_Setting.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch(position){
                    case 0:
                        createFileListDialog(filefolder.toString(),"");
                        break;
                    case 1:
                        if(prelogin.getBoolean("loginornot",false)){
                            prelogin.edit().putBoolean("loginornot",false).commit();
                            Toast.makeText(getActivity(),"已登出系統",Toast.LENGTH_SHORT).show();}
                        else{
                            Toast.makeText(getActivity(),"尚未登入",Toast.LENGTH_SHORT).show();
                        }
                        FragmentTransaction transaction=getFragmentManager().beginTransaction();
                        transaction.replace(R.id.center,new LoginFragment()).commit();
                        break;




                }
            }
        });


    }

    public class Myadapter extends BaseAdapter{
        private LayoutInflater myInflater;
        public Myadapter(Context c){
            myInflater=LayoutInflater.from(c);
        }
        @Override
        public int getCount(){
            return list_head.length;
        }
        @Override
        public Object getItem(int position){
            return list_head[position];
        }
        @Override
        public long getItemId(int postion){
            return postion;
        }
        @Override
        public View getView(int posiition,View convertView,ViewGroup parent){
            convertView=myInflater.inflate(R.layout.list_setting,null);

            TextView txt_listhead=(TextView)convertView.findViewById(R.id.txt_listhead);
            TextView txt_listsub=(TextView)convertView.findViewById(R.id.txt_listsub);

            txt_listhead.setText(list_head[posiition]);
            txt_listsub.setText(list_sub[posiition]);

            return convertView;

        }

    }

    private Runnable refreshrun=new Runnable() {
        @Override
        public void run() {
            listst_foldernow=txt_foldernow;
            list_sub=new String[]{listst_foldernow,""};
            myadapter.notifyDataSetChanged();
            list_Setting.setAdapter(myadapter);
            list_Setting.invalidateViews();
            System.out.println( listst_foldernow);

        }
    };
    private void createFileListDialog(String filepath , String filename){
        final File mypath = new File(filepath,filename);
        File[] files = mypath.listFiles();
        txt_foldernow=mypath.toString();
        ArrayList<String> arrayList= new ArrayList<>();
        for(int i = 0 ; i < files.length; i++){
            if(!files[i].isFile()){
                arrayList.add(files[i].getName());
            }
        }
        final AlertDialog.Builder addressDialog=new AlertDialog.Builder(getActivity());
        addressDialog.setTitle(txt_foldernow);
        addressDialog.setPositiveButton("確定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                handler.post(refreshrun);
            }
        });
        addressDialog.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        addressDialog.setNeutralButton("上層", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        final ArrayAdapter<String> addressadapter=new ArrayAdapter<String>(getActivity(),android.R.layout.select_dialog_item,arrayList);
        addressDialog.setAdapter(addressadapter,new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog,int which){
                createFileListDialog(mypath.getPath().toString(),addressadapter.getItem(which).toString());
            }
        });
        addressDialog.show();
    }

    @Override
    public void onPause(){
        super.onPause();
        presavefileadr.edit()
                .putString("Savefile",listst_foldernow)
        .commit();

    }
}
