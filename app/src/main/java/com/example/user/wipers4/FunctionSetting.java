package com.example.user.wipers4;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.kyleduo.switchbutton.SwitchButton;

import java.io.IOException;
import java.net.URL;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class FunctionSetting extends Fragment {
    private String testnum,watertime,angle,rate,temp,open_temp,open_wtr,wettimes_pre,wettimes,waittimes,drytimes;
    private EditText edt_testnum,edt_watertime,edt_angle,edt_temp,edt_wettimes_pre,edt_wettimes,edt_waittimes,edt_drytimes;
    private SwitchButton swh_speed,swh_degreeopen,swh_wateropen;
    private ImageButton btn_wipeone,btn_water,btn_stop,btn_start,btn_set;
    private OkHttpClient client=new OkHttpClient();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_function_setting, container, false);
    }

    @Override
    public void onViewCreated(View view,Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView title=(TextView)getActivity().findViewById(R.id.action_bar_title);
        title.setText("機台設定");

        edt_testnum=(EditText) getView().findViewById(R.id.edt_testnum);
        edt_watertime=(EditText) getView().findViewById(R.id.edt_watertime);
//        edt_angle=(TextView) getView().findViewById(R.id.edt_angle);
//        edt_temp=(TextView) getView().findViewById(R.id.edt_degree);
        edt_wettimes_pre=(EditText) getView().findViewById(R.id.edt_wettimes_pre);
        edt_wettimes=(EditText) getView().findViewById(R.id.edt_wettimes);
        edt_waittimes=(EditText) getView().findViewById(R.id.edt_waittimes);
        edt_drytimes=(EditText) getView().findViewById(R.id.edt_drytimes);
        swh_speed=(SwitchButton) getView().findViewById(R.id.swh_speed);
        swh_degreeopen=(SwitchButton) getView().findViewById(R.id.swh_degreeopen);
        swh_wateropen=(SwitchButton) getView().findViewById(R.id.swh_wateropen);
        btn_wipeone=(ImageButton) getView().findViewById(R.id.btn_wipeone);
        btn_water=(ImageButton) getView().findViewById(R.id.btn_water);
        btn_stop=(ImageButton) getView().findViewById(R.id.btn_stop);
        btn_start=(ImageButton) getView().findViewById(R.id.btn_start);
        btn_set=(ImageButton)getView().findViewById(R.id.btn_set);




        CheckState();


        swh_speed.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    rate="1";
                }
                else{
                    rate="0";
                }
            }
        });
        swh_degreeopen.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    open_temp="1";
                }
                else{
                    open_temp="0";
                }
            }
        });
        swh_wateropen.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    open_wtr="1";
                }
                else{
                    open_wtr="0";
                }
            }
        });

        btn_wipeone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                String urlstart="http://wiper.gevsi-tech.com/getfilelist.php?Mode=0&number=1&angle="+angle+"&rate="+rate+"&water="+watertime;
//                Request request = new Request.Builder().url(urlstart).build();
//                Call call = client.newCall(request);
//                call.enqueue(new Callback() {
//                    @Override
//                    public void onFailure(Call call, IOException e) {
//                        getActivity().runOnUiThread(new Runnable() {
//                            public void run() {
//                                Toast.makeText(getActivity(), "網路尚未連接，或連線失敗!", Toast.LENGTH_SHORT).show();
//                            }
//                        });
//
//
//                    }
//                    @Override
//                    public void onResponse(Call call, Response response) throws IOException {
//                        String json = response.body().string();
//                        String[] state= json.replace("[", "").replace("]", "").replaceAll("\"", "").split(",");
//                        switch(state[0]){
//                            case "state=0":
//                                getActivity().runOnUiThread(new Runnable() {
//                                    public void run() {
//                                        Toast.makeText(getActivity(), "執行成功!", Toast.LENGTH_SHORT).show();
//                                    }
//                                });
//                                break;
//                            case "state=1":
//                                final String renumber=state[1].split("=")[1];
//                                String rewater=state[4].split("=")[1];
//                                switch(rewater){
//                                    case "0":
//                                        rewatertime="<30秒";
//                                        break;
//                                    case "1":
//                                        rewatertime="30~60秒";
//                                        break;
//                                    case "2":
//                                        rewatertime="60~90秒";
//                                        break;
//                                    case "3":
//                                        rewatertime="90~120秒";
//                                        break;
//                                }
//                                getActivity().runOnUiThread(new Runnable() {
//                                    public void run() {
//                                        Toast.makeText(getActivity(), "尚有動作執行!剩餘次數="+renumber+"次,剩餘噴水時間="+rewatertime, Toast.LENGTH_SHORT).show();
//                                    }
//                                });
//                                break;
//
//                        }
//
//
//                    }
//                });
            }
        });

        btn_water.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                String urlstart="http://wiper.gevsi-tech.com/getfilelist.php?Mode=1&watertime"+watertime;
//                Request request = new Request.Builder().url(urlstart).build();
//                Call call = client.newCall(request);
//                call.enqueue(new Callback() {
//                    @Override
//                    public void onFailure(Call call, IOException e) {
//                        getActivity().runOnUiThread(new Runnable() {
//                            public void run() {
//                                Toast.makeText(getActivity(), "網路尚未連接，或連線失敗!", Toast.LENGTH_SHORT).show();
//                            }
//                        });
//
//
//                    }
//                    @Override
//                    public void onResponse(Call call, Response response) throws IOException {
//                        String json = response.body().string();
//                        String[] state= json.replace("[", "").replace("]", "").replaceAll("\"", "").split(",");
//                        switch(state[0]){
//                            case "state=0":
//                                getActivity().runOnUiThread(new Runnable() {
//                                    public void run() {
//                                        Toast.makeText(getActivity(), "執行成功!", Toast.LENGTH_SHORT).show();
//                                    }
//                                });
//                                break;
//                            case "state=1":
//                                final String renumber=state[1].split("=")[1];
//                                String rewater=state[4].split("=")[1];
//                                switch(rewater){
//                                    case "0":
//                                        rewatertime="<30秒";
//                                        break;
//                                    case "1":
//                                        rewatertime="30~60秒";
//                                        break;
//                                    case "2":
//                                        rewatertime="60~90秒";
//                                        break;
//                                    case "3":
//                                        rewatertime="90~120秒";
//                                        break;
//                                }
//                                getActivity().runOnUiThread(new Runnable() {
//                                    public void run() {
//                                        Toast.makeText(getActivity(), "尚有動作執行!剩餘次數="+renumber+"次,剩餘噴水時間="+rewatertime, Toast.LENGTH_SHORT).show();
//                                    }
//                                });
//                                break;
//
//                        }
//
//
//                    }
//                });

            }
        });

        btn_stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String urlstart="http://wiper.gevsi-tech.com/machinecontrol2.php?start=0&search=0";
                Request request = new Request.Builder().url(urlstart).build();
                Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        getActivity().runOnUiThread(new Runnable() {
                            public void run() {
                                Toast.makeText(getActivity(), "網路尚未連接，或連線失敗!", Toast.LENGTH_SHORT).show();
                            }
                        });


                    }
                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        String json = response.body().string();
                        System.out.println("JSON="+json+"123");
                        System.out.println("JSONohohoh="+json.contains("ok"));
                        System.out.println("JSONSTATE="+json.equals(json));
                        System.out.println("Size="+json.length());
                        if(json.replaceAll("\n","").equals("ok")){
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getActivity(), "成功!", Toast.LENGTH_SHORT).show();
                                }
                            });

                        }

                        CheckState();
//                        String[] state= json.replace("[", "").replace("]", "").replaceAll("\"", "").split(",");
//                        switch(state[0]){
//                            case "state=2":
//                                getActivity().runOnUiThread(new Runnable() {
//                                    public void run() {
//                                        Toast.makeText(getActivity(), "成功停止!", Toast.LENGTH_SHORT).show();
//                                    }
//                                });
//                                break;
//                        }


                    }
                });


            }
        });

        btn_set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                testnum=edt_testnum.getText().toString();
                watertime=edt_watertime.getText().toString();
//        angle=edt_angle.getText().toString();
//        temp=edt_temp.getText().toString();
                wettimes_pre=edt_wettimes_pre.getText().toString();
                wettimes=edt_wettimes.getText().toString();
                waittimes=edt_waittimes.getText().toString();
                drytimes=edt_drytimes.getText().toString();
                String urlstart="http://wiper.gevsi-tech.com/machinecontrol2.php?search=2&state=0&times="+testnum+"&degree="+0+"&temp="+0+"&water="+watertime+"&wettimes_pre="+wettimes_pre+"&wettimes="+wettimes+"&waittimes="+waittimes+"&drytimes="+drytimes+"&speed="+rate+"&tempstatus="+open_temp+"&waterstatus="+open_wtr;
                System.out.println(urlstart);
                Request request = new Request.Builder().url(urlstart).build();
                Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        getActivity().runOnUiThread(new Runnable() {
                            public void run() {
                                Toast.makeText(getActivity(), "網路尚未連接，或連線失敗!", Toast.LENGTH_SHORT).show();
                            }
                        });


                    }
                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        String json = response.body().string();
                        CheckState();

                    }
                });
            }
        });

        btn_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String urlstart="http://wiper.gevsi-tech.com/machinecontrol2.php?start=1&search=0";
                Request request = new Request.Builder().url(urlstart).build();
                Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        getActivity().runOnUiThread(new Runnable() {
                            public void run() {
                                Toast.makeText(getActivity(), "網路尚未連接，或連線失敗!", Toast.LENGTH_SHORT).show();
                            }
                        });


                    }
                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        String json = response.body().string();
                        System.out.println("JSON="+json);
                        System.out.println("JSONSTATE="+json.equals("ok"));
                        if(json.replaceAll("\n","").equals("ok")){
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getActivity(), "執行成功!", Toast.LENGTH_SHORT).show();
                                }
                            });


                        }
                        CheckState();
//                        String[] state= json.replace("[", "").replace("]", "").replaceAll("\"", "").split(",");
//                        switch(state[0]){
//                            case "state=0":
//                                getActivity().runOnUiThread(new Runnable() {
//                                    public void run() {
//                                        Toast.makeText(getActivity(), "執行成功!", Toast.LENGTH_SHORT).show();
//                                    }
//                                });
//                                break;
//                            case "state=1":
//                                final String renumber=state[1].split("=")[1];
//                                String rewater=state[4].split("=")[1];
//                                switch(rewater){
//                                    case "0":
//                                        rewatertime="<30秒";
//                                        break;
//                                    case "1":
//                                        rewatertime="30~60秒";
//                                        break;
//                                    case "2":
//                                        rewatertime="60~90秒";
//                                        break;
//                                    case "3":
//                                        rewatertime="90~120秒";
//                                        break;
//                                }
//                                getActivity().runOnUiThread(new Runnable() {
//                                    public void run() {
//                                        Toast.makeText(getActivity(), "尚有動作執行!剩餘次數="+renumber+"次,剩餘噴水時間="+rewatertime, Toast.LENGTH_SHORT).show();
//                                    }
//                                });
//                                break;
//
//                        }


                    }
                });


            }
        });
    }

    public void CheckState(){
        String urlstart="http://wiper.gevsi-tech.com/machinecontrol2.php?start=0&search=1";
        Request request = new Request.Builder().url(urlstart).build();
        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(getActivity(), "網路尚未連接，或連線失敗!", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String json = response.body().string();
                final String[] state= json.split(",");
                System.out.println(json);
                final Response resPonse=response;
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            System.out.println(state[11]);
                            if(state[11].equals("0")){
                                btn_stop.setEnabled(false);
                                btn_start.setEnabled(true);
                            }
                            else{
                                btn_stop.setEnabled(true);
                                btn_start.setEnabled(false);
                            }

                            edt_testnum.setText(state[0]);
//                            edt_angle.setText(state[1]);
                            if(state[2].equals("1")){
                                swh_speed.setChecked(true);
                            }else{swh_speed.setChecked(false);}
//                            edt_temp.setText(state[3]);
                            edt_watertime.setText(state[4]);
                            if(state[5].equals("1")){
                                swh_degreeopen.setChecked(true);
                            }else{
                                swh_degreeopen.setChecked(false);
                            }
                            if(state[6].equals("1")){
                                swh_wateropen.setChecked(true);
                            }else{
                                swh_wateropen.setChecked(false);
                            }
                            edt_waittimes.setText(state[7]);
                            edt_wettimes.setText(state[8]);
                            edt_wettimes_pre.setText(state[9]);
                            edt_drytimes.setText(state[10]);
                        }catch(Exception e){
                            e.printStackTrace();
                        }

                    }
                });




//                if(json.replaceAll("\n","").equals("0")){
//                    getActivity().runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            btn_stop.setEnabled(false);
//                            btn_start.setEnabled(true);
//                        }
//                    });
//                }else{
//                    getActivity().runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            btn_stop.setEnabled(true);
//                            btn_start.setEnabled(false);
//                        }
//                    });
//                }

            }
        });
    }
}
