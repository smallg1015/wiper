package com.example.user.wipers4;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;


import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity{
    private int mYear,mMonth,mDay;
    private int pYear,pMonth,pDay;
    private SharedPreferences preday;
    private SharedPreferences prelogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        preday=getSharedPreferences("preday",MODE_PRIVATE);
        prelogin=getSharedPreferences("prelogin",MODE_PRIVATE);

        Timer timer=new Timer();
        timer.schedule(timerTask,1000);

        Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH)+1;
        mDay = c.get(Calendar.DAY_OF_MONTH);



        c.set(mYear,mMonth,mDay);

        System.out.println(mYear);
        System.out.println(mMonth);
        System.out.println(mDay);

        System.out.println(preday.getInt("Year",mYear));
        System.out.println(preday.getInt("Month",mMonth));
        System.out.println(preday.getInt("Day",mDay));




        Calendar pc=Calendar.getInstance();
        pc.set(preday.getInt("Year",mYear),preday.getInt("Month",mMonth),preday.getInt("Day",mDay));

        int betweendate=(int)(c.getTimeInMillis()-pc.getTimeInMillis())/(1000*60*60*24);
        System.out.println(betweendate);

        if(betweendate>5){
            prelogin.edit().putBoolean("loginornot",false);
        }
















    }
    private TimerTask timerTask=new TimerTask() {
        @Override
        public void run() {
            Intent launchNextActivity;
            launchNextActivity = new Intent(MainActivity.this,Main2Activity.class);
            launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(launchNextActivity);
        }
    };



}
