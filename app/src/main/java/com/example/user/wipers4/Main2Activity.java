package com.example.user.wipers4;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Handler;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;

import java.util.Calendar;

public class Main2Activity extends AppCompatActivity {

    private SharedPreferences prelogin;
    private SharedPreferences preday;
    private AHBottomNavigation bottomNavigation;
    private Boolean loginornot;
    private int mYear,mMonth,mDay;
    private SharedPreferences whatfragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        View view = getLayoutInflater().inflate(R.layout.toolbar,
                null);
        android.support.v7.app.ActionBar.LayoutParams layoutParams = new android.support.v7.app.ActionBar.LayoutParams(android.support.v7.app.ActionBar.LayoutParams.MATCH_PARENT,
                android.support.v7.app.ActionBar.LayoutParams.MATCH_PARENT);
        actionBar.setCustomView(view, layoutParams);
        Toolbar parent = (Toolbar) view.getParent();
        parent.setContentInsetsAbsolute(0, 0);










        bottomNavigation = (AHBottomNavigation) findViewById(R.id.bottom_navigation);

        prelogin=getSharedPreferences("prelogin",MODE_PRIVATE);
        preday=getSharedPreferences("preday",MODE_PRIVATE);
        whatfragment=getSharedPreferences("whatfragment",MODE_PRIVATE);


        //取的目前日期
        Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH)+1;
        mDay = c.get(Calendar.DAY_OF_MONTH);








        // Create items
        AHBottomNavigationItem item1 = new AHBottomNavigationItem("首頁 ", R.drawable.home, R.color.colorPrimary);
        AHBottomNavigationItem item2 = new AHBottomNavigationItem("檔案", R.drawable.file, R.color.colorPrimary);
        AHBottomNavigationItem item3 = new AHBottomNavigationItem("機台設定", R.drawable.machine,R.color.colorPrimary);
        AHBottomNavigationItem item4 = new AHBottomNavigationItem("設定", R.drawable.settingicon, R.color.colorPrimary);

        // Add items
        bottomNavigation.addItem(item1);
        bottomNavigation.addItem(item2);
        bottomNavigation.addItem(item3);
        bottomNavigation.addItem(item4);

        // Set background color
        bottomNavigation.setDefaultBackgroundColor(Color.parseColor("#FF37373F"));
        bottomNavigation.setAccentColor(Color.parseColor("#FF5BE824"));
        bottomNavigation.setInactiveColor(Color.parseColor("#FFFFFF"));

        bottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW);



        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        loginornot=prelogin.getBoolean("loginornot",false);
        if(loginornot) {

            transaction.replace(R.id.center, new HomeFragment());
            transaction.commit();
        }else{
            transaction.replace(R.id.center, new LoginFragment());
            transaction.commit();
        }


        bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {
                loginornot=prelogin.getBoolean("loginornot",false);

                switch (position) {
                    case 0: {
                        whatfragment.edit().putString("fragment","0").commit();
                        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                        if(loginornot) {
                            transaction.replace(R.id.center, new HomeFragment());
                            transaction.commit();
                        }else {
                            transaction.replace(R.id.center, new LoginFragment());
                            transaction.commit();
                        }




                    }
                    break;
                    case 1: {
                        whatfragment.edit().putString("fragment","1").commit();
                        FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
                        if(loginornot) {

                            transaction.replace(R.id.center, new FileFragment());
                            transaction.commit();
                        }
                        else{
                            transaction.replace(R.id.center, new LoginFragment());
                            transaction.commit();
                        }


                    }
                    break;
                    case 2: {
                        whatfragment.edit().putString("fragment","2").commit();
                        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                        if(loginornot) {

                            transaction.replace(R.id.center, new FunctionSetting());
                            transaction.commit();
                        }
                        else{
                            transaction.replace(R.id.center, new LoginFragment());
                            transaction.commit();
                        }



                    }
                    break;
                    case 3: {
                        whatfragment.edit().putString("fragment","3").commit();
                        FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
                        if(loginornot) {
                            transaction.replace(R.id.center, new SettingFragment());
                            transaction.commit();
                        }
                        else{
                            transaction.replace(R.id.center, new LoginFragment());
                            transaction.commit();
                        }



                    }
                    break;

                }
                // Do something cool here...
                return true;
            }
        });


    }

    private Boolean exit = false;
    @Override
    public void onBackPressed() {
        if (exit) {
            finish(); // finish activity
        } else {
            Toast.makeText(this, "Press Back again to Exit.",
                    Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);

        }

    }

    @Override
    public void onStop(){
        super.onStop();
        preday.edit().putInt("Year",mYear).putInt("Month",mMonth).putInt("Day",mDay).commit();
    }

}
