package com.example.user.wipers4;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Environment;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class LineChartActivity extends AppCompatActivity {
    private ImageView image;
    private SharedPreferences presavefileadr,predatetime;
    private LineChart linechart,linechart2,linechart3;
    private Spinner savespn;
    private File filechart,filechart2,filechart3,fileimage,filefolder,filejson;
    private List<String> folderlist;
    private Button address_btn,save_btn;
    private EditText address_edit;
    private String Foldernow;
    private CheckBox line1_box,line2_box,line3_box,line4_box,ch2_line1_box,ch2_line2_box,ch2_line3_box,ch2_line4_box,ch3_line1_box,ch3_line2_box,ch3_line3_box,ch3_line4_box;
    private int csvnumber,jpgnumber,filenumber;
    private String[] str_jpgname=new String[]{"二質化","原圖"};
    private int[] colorarray=new int[]{Color.BLUE,Color.GREEN,Color.RED,Color.YELLOW,Color.MAGENTA,Color.BLACK};
    private String[] Linename={"Line1","Line2","Line3","Line4","Line5","Line6"};
    private volatile boolean createLag=true;
    private RelativeLayout layout1,layout2,layout3,layout4,layout5,layout6,layout7,layout8,layout9,layout10;
    private volatile boolean lineboxlag=false;

    ArrayList<ArrayList<ArrayList<Entry>>> ListListListEntry;
    ArrayList<ArrayList<String>> labels;
    ArrayList<ArrayList<ILineDataSet>> dataSetsList;
    ArrayList<LineChart> ListChart;
    ArrayList<LineData> ListLineData;
    ArrayList<RelativeLayout> ListLayout;
    ArrayList<String> ListLinename;



    ArrayList<ArrayList<Entry>> Listlist;
    ArrayList<ArrayList<Entry>> Listlist2;
    ArrayList<ArrayList<Entry>> Listlist3;


    ArrayList<String> labels2;
    ArrayList<String> labels3;

    LineData data,data2,data3;
    private SliderLayout mSlider;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_line_chart);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        View view = getLayoutInflater().inflate(R.layout.toolbar,
                null);
        android.support.v7.app.ActionBar.LayoutParams layoutParams = new android.support.v7.app.ActionBar.LayoutParams(android.support.v7.app.ActionBar.LayoutParams.MATCH_PARENT,
                android.support.v7.app.ActionBar.LayoutParams.MATCH_PARENT);
        actionBar.setCustomView(view, layoutParams);
        Toolbar parent = (Toolbar) view.getParent();
        parent.setContentInsetsAbsolute(0, 0);

        TextView title=(TextView)view.findViewById(R.id.action_bar_title);
        title.setText("圖表");




        //元件初始化區
        //實驗區
        linechart=new LineChart(this);
        linechart2=new LineChart(this);
        linechart3=new LineChart(this);
        layout1=(RelativeLayout)findViewById(R.id.chart);
        layout2=(RelativeLayout)findViewById(R.id.chart2);

        layout3=(RelativeLayout)findViewById(R.id.chart3);
        layout4=(RelativeLayout)findViewById(R.id.chart4);
        layout5=(RelativeLayout)findViewById(R.id.chart5);
        layout6=(RelativeLayout)findViewById(R.id.chart6);
        layout7=(RelativeLayout)findViewById(R.id.chart7);
        LinearLayout check1=(LinearLayout)findViewById(R.id.check1);
        LinearLayout check2=(LinearLayout)findViewById(R.id.check2);



//        line1_box=(CheckBox)findViewById(R.id.ch1_line1_box);
//        line2_box=(CheckBox)findViewById(R.id.ch1_line2_box);
//        line3_box=(CheckBox)findViewById(R.id.ch1_line3_box);
//        line4_box=(CheckBox)findViewById(R.id.ch1_line4_box);
//        ch2_line1_box=(CheckBox)findViewById(R.id.ch2_line1_box);
//        ch2_line2_box=(CheckBox)findViewById(R.id.ch2_line2_box);
//        ch2_line3_box=(CheckBox)findViewById(R.id.ch2_line3_box);
//        ch2_line4_box=(CheckBox)findViewById(R.id.ch2_line4_box);
//        ch3_line1_box=(CheckBox)findViewById(R.id.ch3_line1_box);
//        ch3_line2_box=(CheckBox)findViewById(R.id.ch3_line2_box);
//        ch3_line3_box=(CheckBox)findViewById(R.id.ch3_line3_box);
//        ch3_line4_box=(CheckBox)findViewById(R.id.ch3_line4_box);5
        mSlider=(SliderLayout)findViewById(R.id.slider);

//        try {
//            JSONObject obj = new JSONObject(loadJSONFromAsset());
//            JSONArray m_jArry = obj.getJSONArray("formules");











        presavefileadr=getSharedPreferences("SaveAddress",MODE_PRIVATE);
        presavefileadr.getString("Savefile","");


        predatetime=getSharedPreferences("predatetime",MODE_PRIVATE);

        ListListListEntry=new ArrayList<ArrayList<ArrayList<Entry>>>();
        labels = new ArrayList<ArrayList<String>>();
        dataSetsList=new ArrayList<ArrayList<ILineDataSet>>();
        ListChart=new ArrayList<LineChart>();
        ListLineData=new ArrayList<LineData>();
        ListLayout=new ArrayList<RelativeLayout>();
        ListLinename=new ArrayList<String>();

        //實驗區

        ListLayout.add(layout1);
        ListLayout.add(layout2);
        ListLayout.add(layout3);
        ListLayout.add(layout4);
        ListLayout.add(layout5);
        ListLayout.add(layout6);
        ListLayout.add(layout7);
        System.out.println("chartcont="+ListChart.size());;



        filefolder=new File(Environment.getExternalStorageDirectory().getPath()+"/Wiper"+File.separator+predatetime.getString("date","")+File.separator+predatetime.getString("time",""));
        filejson=new File(filefolder.getPath()+"/setting.json");
        System.out.println(filefolder.toString());


        try {
            JSONObject obj = new JSONObject(loadJSONFromAsset(filejson));
            JSONObject titleob = obj.getJSONObject("title");

            String name_a=titleob.getString("a");
            String name_b=titleob.getString("b");
            String name_c=titleob.getString("c");
            String name_d=titleob.getString("d");
            String name_e=titleob.getString("e");
            String name_f=titleob.getString("f");

            ListLinename.add(name_a);
            ListLinename.add(name_b);
            ListLinename.add(name_c);
            ListLinename.add(name_d);
            ListLinename.add(name_e);
            ListLinename.add(name_f);

        } catch (JSONException e) {
                    e.printStackTrace();
        }



        csvnumber=0;
        jpgnumber=0;
        File[] filelist=filefolder.listFiles();
        System.out.println(filelist.length);
        filenumber=0;//檔案代號
        for(int i=0;i< filelist.length;i++) {


            File filewhat=filelist[i];
            System.out.println(filewhat.toString().split("\\.")[1]);

            ///若讀取的檔案類型為csv
            if (filewhat.toString().split("\\.")[1].equals("csv")) {
                //代表第幾個csv檔
                csvnumber++;
                System.out.println("csvnumber=" + csvnumber);
                //新增一個csv檔行資料(xy資料)集合
                ListChart.add(new LineChart(this));
                ListListListEntry.add(new ArrayList<ArrayList<Entry>>());
                //新增一個csv檔的linedataset集合
                dataSetsList.add(new ArrayList<ILineDataSet>());
                //新增一個csv檔的x標籤集合
                labels.add(new ArrayList<String>());
                //除錯
                System.out.println("LISTLISTLIST:" + ListListListEntry.size());
                System.out.println("listdaraset:" + dataSetsList.size());
                System.out.println("labels:" + labels.size());
                System.out.println("Chart:" + ListChart.size());
                //匯入csv擋,讀取資料
                try {
                    //讀取第i個csv檔案
                    BufferedReader bufferedReader = new BufferedReader(new FileReader(filewhat));
                    String line = "";

                    int rownumber = 0;
                    line = bufferedReader.readLine();
                    String[] csvdata = line.split(",");
                    System.out.println(csvdata.length);
                    //判斷csv檔有幾行,加入幾個ArrayList<Entry>)
                    for (int j = 0; j < csvdata.length; j++) {
                        ListListListEntry.get(csvnumber - 1).add(new ArrayList<Entry>());
                    }

                    System.out.println((line = bufferedReader.readLine()) != null);
                    while ((line = bufferedReader.readLine()) != null) {
                        //第幾個資料
                        rownumber++;
                        //第csvnumber-1個csv檔代表X軸標籤的ArrayList<String>新增字元(0.1.2.....)
                        labels.get(csvnumber - 1).add(String.valueOf(rownumber - 1));
                        csvdata = line.split(",");
                        for (int h = 0; h < csvdata.length; h++) {
                            //對每一行ArryaList<Entry>加入entry資料

                            ListListListEntry.get(csvnumber - 1).get(h).add(new Entry(Float.parseFloat(csvdata[h]), rownumber - 1));
                        }
                    }
                    //依據行資料建立LineDataSet並新增到第幾個csv檔的Dataset集合中
                    for (int f = 0; f < csvdata.length; f++) {

                        dataSetsList.get(csvnumber - 1).add(createDataSet(ListListListEntry.get(csvnumber - 1).get(f),ListLinename.get(f), colorarray[f]));
                    }
                    //建立csv檔的LineData
                    ListLineData.add(new LineData(labels.get(csvnumber - 1), dataSetsList.get(csvnumber - 1)));
                    System.out.println(ListLineData.size());

                    //根據第幾個csv檔建立linechart並設定


                    System.out.println("here sis chart create");
                    ListChart.get(csvnumber - 1).clear();
                    ListChart.get(csvnumber - 1).setData(ListLineData.get(csvnumber - 1));
                    ListChart.get(csvnumber - 1).refreshDrawableState();
                    Chartsetting(ListChart.get(csvnumber - 1));

                    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

                    ListLayout.get(csvnumber-1).addView(ListChart.get(csvnumber - 1),params);














                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
                        //若讀取的檔案類型為jpg
            else if (filewhat.toString().split("\\.")[1].equals("jpg")) {
                jpgnumber++;
                HashMap<String, File> file_maps = new HashMap<String, File>();
                file_maps.put(str_jpgname[jpgnumber - 1], new File(filewhat.toString()));


                for (String name : file_maps.keySet()) {
                    final TextSliderView textSliderView = new TextSliderView(LineChartActivity.this);
                    // initialize a SliderLayout
                    textSliderView
                            .description(name)
                            .image(file_maps.get(name))
                            .setScaleType(BaseSliderView.ScaleType.Fit)
                            .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                                @Override
                                public void onSliderClick(BaseSliderView slider) {

                                }
                            });

                    //add your extra information
                    textSliderView.bundle(new Bundle());
                    textSliderView.getBundle()
                            .putString("extra", name);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            System.out.println("here is photo create");
                            mSlider.addSlider(textSliderView);
                            mSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
                            mSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                            mSlider.setCustomAnimation(new DescriptionAnimation());
                            mSlider.addOnPageChangeListener(new ViewPagerEx.OnPageChangeListener() {
                                @Override
                                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                                }

                                @Override
                                public void onPageSelected(int position) {

                                }

                                @Override
                                public void onPageScrollStateChanged(int state) {

                                }
                            });

                        }
                    });


                }

            }
//            else if(filewhat.toString().split("\\.")[1].equals("json")){
//
//
//            }


        }

        System.out.println("csvnumber="+csvnumber);



        if(csvnumber<ListLayout.size()){
            for(int m=csvnumber;m<ListLayout.size();m++){
                ListLayout.get(m).setVisibility(RelativeLayout.GONE);
            }
        }

        check1.setVisibility(LinearLayout.GONE);





    }




    //建立圖表線資料區
    private LineDataSet createDataSet(ArrayList<Entry> whichlist, String linename, int color){
        LineDataSet dataset=new LineDataSet(whichlist,linename);
        dataset.setDrawValues(false);
        dataset.setDrawCircles(false);
        dataset.setColor(color);

        return dataset;


    }

    private void readcsvtoarray(File chartfile,ArrayList<ArrayList<Entry>> EntryList,ArrayList<String> stringList){

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.barmenu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                for(int s=0;s<csvnumber;s++){
                    ListChart.get(s).setSaveEnabled(true);
                    ListChart.get(s).saveToPath("1",presavefileadr.getString("Savefile","").replace("storage/emulated/0",""));
                    ;
                }

                Toast.makeText(LineChartActivity.this,"以儲存至:"+presavefileadr.getString("Savefile",""),Toast.LENGTH_LONG).show();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public void Chartsetting(LineChart linechart){
        linechart.getAxisRight().setDrawLabels(false);
        linechart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        linechart.getXAxis().setDrawLimitLinesBehindData(true);
        linechart.getAxisLeft().setDrawZeroLine(false);
        linechart.getAxisLeft().setSpaceBottom(0f);
        linechart.getAxisRight().setSpaceBottom(0f);
        linechart.setDescription("");
    }

    public String loadJSONFromAsset(File file) {
        String json = null;
        try {
            InputStream is = new FileInputStream(file);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }



//    public void linechooseboxcreate(final int csvnumberbox, CheckBox box1, CheckBox box2, CheckBox box3, CheckBox box4){
//        int i=csvnumberbox-1;
//        box1.setChecked(true);
//        box2.setChecked(true);
//        box3.setChecked(true);
//        box4.setChecked(true);
//
//        box1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                int i=csvnumberbox-1;
//                if(isChecked) {
//                    if (lineboxlag) {
//                        dataSetsList.get(i).add(createDataSet(ListListListEntry.get(i).get(0), "Line1", colorarray[0]));
//                    }
//                }
//                else{
//                    dataSetsList.get(i).remove(ListLineData.get(i).getDataSetByLabel("Line1",true));
//                    lineboxlag=true;
//                }
//            }
//        });
//        box2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                int i=csvnumberbox;
//                if(isChecked){
//                    if(lineboxlag) {
//                        dataSetsList.get(i).add(createDataSet(ListListListEntry.get(i).get(1), "Line2", colorarray[1]));
//                    }
//                }else{
//                    dataSetsList.get(i).remove(ListLineData.get(i).getDataSetByLabel("Line2",true));
//                    lineboxlag=true;
//                }
//            }
//        });
//        box3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                int i=csvnumberbox;
//                if(isChecked){
//                    if(lineboxlag) {
//                        dataSetsList.get(i).add(createDataSet(ListListListEntry.get(i).get(2), "Line3", colorarray[2]));
//                    }
//                }
//                else{
//                    dataSetsList.get(i).remove(ListLineData.get(i).getDataSetByLabel("Line3",true));
//                    lineboxlag=true;
//                }
//            }
//        });
//        box4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                int i=csvnumberbox;
//                if(isChecked){
//                    if(lineboxlag) {
//                        dataSetsList.get(i).add(createDataSet(ListListListEntry.get(i).get(3), "Line4", colorarray[3]));
//                    }
//                    }
//                else{
//                    dataSetsList.get(i).remove(ListLineData.get(i).getDataSetByLabel("Line4",true));
//                    lineboxlag=true;
//                }
//            }
//        });
//
//
//
//    }




}




