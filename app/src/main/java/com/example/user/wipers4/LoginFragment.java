package com.example.user.wipers4;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class LoginFragment extends Fragment {
    private SharedPreferences prelogin;
    private EditText edt_name,edt_password;
    private Button btn_login;
    private SharedPreferences whatfragment;
    private OkHttpClient client=new OkHttpClient();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onViewCreated(View view,Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView title=(TextView)getActivity().findViewById(R.id.action_bar_title);
        title.setText("登入");
        edt_name=(EditText)getView().findViewById(R.id.edt_name);
        edt_password=(EditText)getView().findViewById(R.id.edt_password);
        btn_login=(Button)getView().findViewById(R.id.btn_login);


        prelogin=getActivity().getSharedPreferences("prelogin",Context.MODE_PRIVATE);
        whatfragment=getActivity().getSharedPreferences("whatfragment",Context.MODE_PRIVATE);





        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String urlstart="http://wiper.gevsi-tech.com/login.php?id="+edt_name.getText().toString()+"&password="+edt_password.getText().toString();
                System.out.println(urlstart);
                Request request = new Request.Builder().url(urlstart).build();
                Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        getActivity().runOnUiThread(new Runnable() {
                            public void run() {
                                Toast.makeText(getActivity(), "網路尚未連接，或連線失敗!", Toast.LENGTH_SHORT).show();
                            }
                        });


                    }
                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        String json = response.body().string();
                        System.out.println(json);
                        System.out.println(json="Success");
                        if(json.equals("Success")){
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getActivity(),"登入成功",Toast.LENGTH_LONG).show();
                                    prelogin.edit()
                                            .putBoolean("loginornot",true)
                                            .commit();
                                    FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                                    switch (whatfragment.getString("fragment","0")) {
                                        case "0":
                                            transaction.replace(R.id.center,new HomeFragment());
                                            transaction.commit();
                                            break;
                                        case "1":
                                            transaction.replace(R.id.center,new FileFragment());
                                            transaction.commit();
                                            break;
                                        case "2":
                                            transaction.replace(R.id.center,new FunctionSetting());
                                            transaction.commit();
                                            break;
                                        case "3":
                                            transaction.replace(R.id.center,new SettingFragment());
                                            transaction.commit();
                                            break;

                                    }
                                }
                            });


                        }else{
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getActivity(),"帳號或密碼錯誤",Toast.LENGTH_LONG).show();
                                }
                            });

                            prelogin.edit()
                                    .putBoolean("loginornot",false)
                                    .commit();
                        }
                    }
                });

            }
        });





    }
}
