package com.example.user.wipers4;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Handler;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static android.R.attr.path;
import static android.R.attr.start;


public class
FileFragment extends Fragment {
    private SharedPreferences predatetime;

    private String[] datearray;
    private String[] filearray;
    private String url,urldate,urltime,urltimefile,state,nowdate,nowtime,prewhat;
    String filenameplusform,filename,fileform;
    private ListView list_file;
    private EditText edt_date;
    private Button btn_date;
    private int downloadnumber;

    private OkHttpClient client=new OkHttpClient();
    private Button button;
    private Spinner spn_date;
    private Handler handler;
    private volatile boolean Savelag=false;
    private volatile boolean Sendlag=true;
    private Timer downloadTimer;


    ArrayAdapter<String> dateAdapter;
    private int mYear, mMonth, mDay,muchfile;


    private String filesave;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_file, container, false);
    }

    @Override
    public void onViewCreated(View view,Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView title=(TextView)getActivity().findViewById(R.id.action_bar_title);
        title.setText("檔案");












        //控建初始化區

        list_file=(ListView)getView().findViewById(R.id.list_file);
        btn_date=(Button)getView().findViewById(R.id.btn_date);
        edt_date=(EditText)getActivity().findViewById(R.id.edt_date);


        filesave=Environment.getExternalStorageDirectory().getPath()+"/Wiper";

        predatetime=getActivity().getSharedPreferences("predatetime",Context.MODE_PRIVATE);

        //判斷是否有文件存取權限，有則檢查資料夾是否創建，無則提醒使用者注意。
        requestStoragePermission();


        btn_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog();

            }
        });


        //Listview響應區
        list_file.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                nowtime=list_file.getItemAtPosition(position).toString().replace("時","-").replace("分","-").replace("秒","");
                System.out.println("Nowtime"+nowtime);
                predatetime.edit().putString("time",nowtime).commit();
                Thread thread=new Thread(new Runnable() {
                    @Override
                    public void run() {

                        urltime="http://wiper.gevsi-tech.com/getfilelist.php?Mode=3&date="+nowdate+"&time="+nowtime;

                        //建立連線並讀取連線狀態
                        OkHttpClient okHttpClient;
                        ConnectivityManager CM = (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
                        NetworkInfo info = CM.getActiveNetworkInfo();
                        if(info!=null){
                            state = info.getState().toString();
                        }
                        if(state==null ){
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getActivity() , "網路尚未連接，或連線失敗!",Toast.LENGTH_SHORT).show();
                                }
                            });
                            return;
                        }

                        //由選擇時間的網址讀取時間內檔案的JSON
                        Request request = new Request.Builder().url(urltime).build();
                        Call call = client.newCall(request);
                        call.enqueue(new Callback() {
                            @Override
                            public void onFailure(Call call, IOException e) {
                                getActivity().runOnUiThread(new Runnable() {
                                    public void run() {

                                    }
                                });


                            }
                            @Override
                            public void onResponse(Call call, Response response) throws IOException {
                                String json = response.body().string();

                                filearray = json.replace("[", "").replace("]", "").replaceAll("\"", "").split(",");
                                muchfile=filearray.length;
                                downloadnumber=0;

                                downloadTimer=new Timer();
                                MyTimerTask timerTask=new MyTimerTask();
                                downloadTimer.scheduleAtFixedRate(timerTask, 0, 800);
                                Savelag=false;
                                Sendlag=true;
                                while(Sendlag) {
                                    if(Savelag){Intent intent = new Intent();
                                        intent.setClass(getActivity(), LineChartActivity.class);
                                        startActivity(intent);
                                        Sendlag=false;
                                        Savelag=false;}
                                        }








                            }
                        });

                        //建立相對應的日期及時間資料夾
                        File filedatetime=new File(filesave+File.separator+nowdate+File.separator+nowtime);
                        if(!filedatetime.exists()){
                            filedatetime.mkdirs();
                        }





                    }
                });
                thread.start();


            }
        });








    }

    private class MyTimerTask extends TimerTask{
        @Override
        public void run() {
            if(downloadnumber<muchfile) {
                downloadnumber++;
                String filename = filearray[downloadnumber - 1].split("\\.")[0];
                String fileform = filearray[downloadnumber - 1].split("\\.")[1];

                System.out.println(filename);
                System.out.println(fileform);

                MakeUrl(fileform, filename);
            }else{
                Savelag=true;
                downloadTimer.cancel();
                downloadTimer.purge();
            }
        }
    };

    private void MakeUrl(String form,String filenamehere){
        try {
            String type=null;
            if(form.equals("csv")){type="1";}
            if(form.equals("jpg")){type="2";}
            if(form.equals("json")){type="3";}
           fileform=form;
            filename=filenamehere;
            filenameplusform=filename+"."+fileform;
            System.out.println();



            OkHttpClient okHttpClient=new OkHttpClient();
            urltimefile= "http://wiper.gevsi-tech.com/download.php?type="+type+"&date="+nowdate+"&time="+nowtime+"&files="+filename;
            System.out.println(urltimefile);
            Request request = new Request.Builder()
                    .url(urltimefile)
                    .build();

            Response response = okHttpClient.newCall(request).execute();
            if(!response.body().string().equals("no files")){
                new DownloadFileFromURL().execute(urltimefile);

            }else{
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getActivity(), "下載碼錯誤，請重新確認一次!",Toast.LENGTH_SHORT).show();
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //判斷是否取得權限
    private void requestStoragePermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            int hasPermission = getActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (hasPermission != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},1);
                return;//如果權限未開啟,則跳出取得權限對話框

            }

        }
        File file=new File(filesave);
        if (!file.exists()) {//先判斷目錄存不存在
            file.mkdirs();}
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,String[] permissions, int[] grantResults) {
        if(requestCode==1){


            if(grantResults[0]==PackageManager.PERMISSION_GRANTED){
                File file=new File(filesave);
                if (!file.exists()) {//先判斷目錄存不存在
                    file.mkdirs();}
            }
            else {
                new AlertDialog.Builder(getActivity())
                        .setTitle("警告")
                        .setIcon(R.mipmap.ic_launcher)
                        .setMessage("若不允許存取權限,程式將無法執行功能,是否取得權限?")
                        .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialoginterface, int i) {
                                requestStoragePermission();
                            }
                        })

                        .setNegativeButton("拒絕", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialoginterface, int i) {
                                getActivity().finish();
                            }
                        }).show();
            }

            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }}

    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Bar Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //showDialog(progress_bar_type);
        }

        /**
         * Downloading file in background thread
         * */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();

                // this will be useful so that you can show a tipical 0-100%
                // progress bar
                int lenghtOfFile = conection.getContentLength();

                // download the file
                InputStream input = new BufferedInputStream(url.openStream(),
                        8192);

                // Output stream
                OutputStream output = new FileOutputStream(filesave+File.separator+nowdate+File.separator+nowtime+File.separator+filenameplusform);



                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;

                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    //publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getActivity(), "下載成功",Toast.LENGTH_SHORT).show();

                    }
                });

                // Toast.makeText(MainActivity.this,"ok",Toast.LENGTH_SHORT).show();
                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

    }

    public void showDatePickerDialog() {
        // 設定初始日期
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        // 跳出日期選擇器
        DatePickerDialog dpd = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        // 完成選擇，顯示日期
                        edt_date.setText(setdateform(year,monthOfYear,dayOfMonth));
                        listdatefile();

                    }
                }, mYear, mMonth, mDay);
        dpd.show();
    }

    public String setdateform(int year,int month,int date) {
        String Smonth;
        String Sdate;

        if(String.valueOf(month).length()==1){
            Smonth="0"+String.valueOf(month+1);
        }else{Smonth=String.valueOf(month+1);}
        if(String.valueOf(date).length()==1){
            Sdate="0"+String.valueOf(date);
        }else{Sdate=String.valueOf(date);}

        return String.valueOf(year)+"-"+Smonth+"-"+Sdate;
    }


    public void listdatefile(){
        nowdate=edt_date.getText().toString();
        predatetime.edit().putString("date",nowdate).commit();
        if(!edt_date.getText().toString().equals("")) {
            urldate = "http://wiper.gevsi-tech.com/getfilelist.php?Mode=2&date="+nowdate;
            System.out.println(urldate);
            //由選擇日期的網址讀取JSON
            Request request = new Request.Builder().url(urldate).build();
            Call call = client.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(getActivity(), "網路尚未連接，或連線失敗!", Toast.LENGTH_SHORT).show();
                        }
                    });


                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    String json = response.body().string();
                    final String[] timearray = json.replace("[", "").replace("]", "").replaceAll("\"", "").split(",");
                    for(int h=0;h<timearray.length;h++){
                        timearray[h]=timearray[h].replaceAll("-","分").replaceFirst("分","時")+"秒";

                    }

                    new Thread(new Runnable() {
                        public void run() {
                            //這邊是背景thread在運作, 這邊可以處理比較長時間或大量的運算

                            getActivity().runOnUiThread(new Runnable() {
                                public void run() {
                                    //用讀取出來的字串陣列當作LISTVIEW的選項
                                    ArrayAdapter<String> timeAdapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1, timearray){
                                        @Override
                                        public View getView(int position, View convertView, ViewGroup parent){
                                            // Get the Item from ListView
                                            View view = super.getView(position, convertView, parent);

                                            // Initialize a TextView for ListView each Item
                                            TextView tv = (TextView) view.findViewById(android.R.id.text1);

                                            // Set the text color of TextView (ListView Item)
                                            tv.setTextColor(Color.WHITE);


                                            // Generate ListView Item using TextView
                                            return view;
                                        }

                                    };
                                    list_file.setAdapter(timeAdapter);
                                    list_file.invalidateViews();

                                }
                            });
                        }
                    }).start();
                }
            });
        }
    }
}
