package com.example.user.wipers4;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by user on 2017/6/10.
 */

public class Chart {
    private ArrayList<String> labels;
    private ArrayList<ArrayList<Entry>> Listlist;
    ArrayList<ILineDataSet> dataSets;

    public void creatChart(LineChart linechart, File chartfile){

        labels=new ArrayList<String>();
        Listlist=new ArrayList<ArrayList<Entry>>();

        //匯入csv擋,讀取資料
        try {

            BufferedReader bufferedReader=new BufferedReader(new FileReader(chartfile));
            String line="";

            int rownumber=0;
            line=bufferedReader.readLine();
            String[] csvdata=line.split(",");
            System.out.println(csvdata.length);

            for(int i=0;i<csvdata.length;i++){
                Listlist.add(new ArrayList<Entry>());//判斷csv檔有幾行,加入幾個ArrayList<Entry>)
            }



            while ((line = bufferedReader.readLine()) != null) {
                rownumber++;
                labels.add(String.valueOf(rownumber));
                csvdata = line.split(",");
                for(int j=0;j<csvdata.length;j++) {
                    Listlist.get(j).add(new Entry(Float.parseFloat(csvdata[j]), rownumber));
                }

            }

        }



        catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //圖表設定區
        dataSets = new ArrayList<ILineDataSet>();
        linechart.getAxisRight().setDrawLabels(false);

        linechart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        linechart.getXAxis().setDrawLimitLinesBehindData(true);
        linechart.getAxisLeft().setDrawZeroLine(false);
        linechart.getAxisLeft().setSpaceBottom(0f);
        linechart.getAxisRight().setSpaceBottom(0f);
        linechart.setDescription("");


    }

    public ArrayList<ArrayList<Entry>> getListlist(){
        return Listlist;
    }
    public ArrayList<String> getLabels(){
        return labels;
    }
    public ArrayList<ILineDataSet> getdataSets(){
        return dataSets;
    }
}
